<?php

/**
 * Provide a valid, unique HTML ID.
 */
function inuit_preprocess_region(&$variables) {
  $variables['region'] = drupal_html_id($variables['region']);
}
