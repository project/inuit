; Drush make file for inuit igloos
; `drush make --no-core --working-copy igloos.make.example`

core = 7.x
api = 2

libraries[inuit_breadcrumb][download][type] = git
libraries[inuit_breadcrumb][download][url] = https://github.com/csswizardry/breadcrumb.inuit.css.git
libraries[inuit_breadcrumb][subdir] = inuit_igloos

libraries[inuit_ie6][download][type] = git
libraries[inuit_ie6][download][url] = https://github.com/csswizardry/ie6.inuit.css.git
libraries[inuit_ie6][subdir] = inuit_igloos

libraries[inuit_dropdown][download][type] = git
libraries[inuit_dropdown][download][url] = https://github.com/csswizardry/dropdown.inuit.css.git
libraries[inuit_dropdown][subdir] = inuit_igloos

libraries[inuit_12_col][download][type] = git
libraries[inuit_12_col][download][url] = https://github.com/csswizardry/12-col.inuit.css.git
libraries[inuit_12_col][subdir] = inuit_igloos

libraries[inuit_annotate][download][type] = git
libraries[inuit_annotate][download][url] = https://github.com/csswizardry/annotate.inuit.css.git
libraries[inuit_annotate][subdir] = inuit_igloos

libraries[inuit_keywords][download][type] = git
libraries[inuit_keywords][download][url] = https://github.com/csswizardry/keywords.inuit.css.git
libraries[inuit_keywords][subdir] = inuit_igloos

libraries[inuit_centered_nav][download][type] = git
libraries[inuit_centered_nav][download][url] = https://github.com/csswizardry/centred-nav.inuit.css.git
libraries[inuit_centered_nav][subdir] = inuit_igloos
